test:
	go test ./pkg/...

godep-save:
	rm -rf Godeps/ vendor/ && godep save ./pkg/...

mock:
	mockgen \
		-destination pkg/mock_flocks/mock_flocks.go \
		gitlab.com/arisechurch/go-flocks/pkg/flocks \
		PersonService,GroupService,RosterPositionService,InactivationService \

.PHONY: test godep-save
