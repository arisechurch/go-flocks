package flocks

import (
	"encoding/json"
	"fmt"
	"time"
)

// Check Date implements Marshaler & Unmarshaler
var _ json.Marshaler = &Date{}
var _ json.Unmarshaler = &Date{}

const DateFormat = "2006-01-02"

// Flocks date format
func (d Date) MarshalJSON() ([]byte, error) {
	timeString := fmt.Sprintf(
		"\"%s\"",
		time.Time(d).Format(DateFormat),
	)

	return []byte(timeString), nil
}

func (d *Date) UnmarshalJSON(data []byte) error {
	if string(data) == "null" {
		return nil
	}

	var err error
	var newTime time.Time

	newTime, err = time.Parse(`"`+DateFormat+`"`, string(data))
	if err != nil {
		return nil
	}

	*d = Date(newTime)
	return nil
}

func TimeToDate(t time.Time) Date {
	return Date(time.Date(
		t.Year(),
		t.Month(),
		t.Day(),
		0,
		0,
		0,
		0,
		time.UTC,
	))
}
