package flocks_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-flocks/pkg/flocks"

	"encoding/json"
	"time"
)

func createDate() time.Time {
	return time.Date(2009, time.November, 10, 0, 0, 0, 0, time.UTC)
}

func testFlocks_Time_Date_MarshalJSON(t *testing.T) {
	assert := assert.New(t)

	testTime := createDate()
	ed := flocks.Date(testTime)

	result, err := json.Marshal(ed)
	assert.Nil(err)

	assert.Equal("\"2009-11-10\"", string(result))
}

func testFlocks_Time_Date_UnmarshalJSON(t *testing.T) {
	assert := assert.New(t)

	ed := &flocks.Date{}
	err := json.Unmarshal([]byte("\"2009-11-10\""), ed)
	assert.Nil(err)

	testTime := time.Time(*ed)
	assert.True(testTime.Equal(createDate()))
}

func TestFlocks_Time_Date(t *testing.T) {
	t.Run("MarshalJSON", testFlocks_Time_Date_MarshalJSON)
	t.Run("UnmarshalJSON", testFlocks_Time_Date_UnmarshalJSON)
}
