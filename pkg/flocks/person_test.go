package flocks_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

func testFlocks_Person_PrimaryGroup(t *testing.T) {
	assert := assert.New(t)

	person := &flocks.Person{
		Groups: []*flocks.Group{
			{Name: "non-primary", Primary: false},
			{Name: "primary", Primary: true},
		},
	}

	primaryGroup := person.PrimaryGroup()
	assert.Equal("primary", primaryGroup.Name)
}

func testFlocks_Person_PrimaryGroup_NoGroups(t *testing.T) {
	assert := assert.New(t)

	person := &flocks.Person{}

	primaryGroup := person.PrimaryGroup()
	assert.Nil(primaryGroup)
}

func TestFlocks_Person_PrimaryGroup(t *testing.T) {
	t.Run("OK", testFlocks_Person_PrimaryGroup)
	t.Run("NoGroups", testFlocks_Person_PrimaryGroup_NoGroups)
}
