package flocks

import (
	"time"
)

// Id's
type Id int
type Date time.Time

// Standard options
type Options struct {
	// For limiting scope to a ARISE accounts user
	AriseId Id `url:"arise_id,omitempty"`
}

// Person
type Person struct {
	Id        Id       `json:"id"`
	FirstName string   `json:"first_name"`
	LastName  string   `json:"last_name"`
	Email     string   `json:"email"`
	MobileTel string   `json:"mobile_tel"`
	Gender    string   `json:"gender"`
	Centre    string   `json:"centre"`
	AgeGroup  string   `json:"age_group"`
	Groups    []*Group `json:"groups"`
}

type PersonSearchOptions struct {
	AriseId  Id       `url:"arise_id,omitempty"`
	Query    string   `url:"q,omitempty"`
	Uid      string   `url:"uid,omitempty"`
	Email    string   `url:"email,omitempty"`
	GroupIds []string `url:"group_ids,omitempty"`
}

type PersonService interface {
	Get(id Id, opt *Options) (*Person, error)
	Search(opt *PersonSearchOptions) ([]*Person, error)
}

// Group
type Group struct {
	Id            Id      `json:"id"`
	Name          string  `json:"name"`
	Category      string  `json:"category"`
	CentreName    string  `json:"centre_name"`
	AgeGroupName  string  `json:"age_group_name"`
	LeadersNames  string  `json:"leaders_names"`
	LeadersEmails string  `json:"leaders_emails"`
	Latitude      float32 `json:"latitude"`
	Longitude     float32 `json:"longitude"`
	Day           string  `json:"day"`
	Frequency     string  `json:"frequency"`
	Time          string  `json:"time"`
	Topic         string  `json:"topic"`
	Suburb        string  `json:"suburb"`
	GroupType     string  `json:"group_type"`
	Leader        bool    `json:"leader"`
	Primary       bool    `json:"primary"`
}

type GroupListOptions struct {
	AriseId Id `url:"arise_id,omitempty"`
	// Pointer so we can have nil value
	Leader       *bool  `url:"leader,omitempty"`
	Category     string `url:"category,omitempty"`
	CentreName   string `url:"centre_name,omitempty"`
	AgeGroupName string `url:"age_group_name,omitempty"`
}

type GroupService interface {
	Get(id Id, opt *Options) (*Group, error)
	List(opt *GroupListOptions) ([]*Group, error)
}

type RosterPosition struct {
	Id                Id     `json:"id"`
	RosteredAt        Date   `json:"rostered_at"`
	PersonId          Id     `json:"person_id"`
	ServiceName       string `json:"service_name"`
	PostitionName     string `json:"position_name"`
	LocationName      string `json:"location_name"`
	DepartmentName    string `json:"department_name"`
	SubDepartmentName string `json:"sub_department_name"`
}

type RosterPositionService interface {
	Create(rp *RosterPosition, opt *Options) (*RosterPosition, error)
}

type Inactivation struct {
	Id                 Id         `json:"id"`
	PersonId           Id         `json:"person_id"`
	CreationUserId     Id         `json:"creation_user_id"`
	InactivationReason string     `json:"inactivation_reason"`
	ConfirmationUserId *Id        `json:"confirmation_user_id,omitempty"`
	ConfirmedAt        *time.Time `json:"confirmed_at,omitempty"`
	CreatedAt          *time.Time `json:"created_at,omitempty"`
	UpdatedAt          *time.Time `json:"updated_at,omitempty"`
}

type InactivationListOptions struct {
	StartUpdatedAt time.Time `url:"start_updated_at,omitempty"`
}

type InactivationService interface {
	List(opt *InactivationListOptions) ([]*Inactivation, error)
}

type CheckInType string

const (
	CheckInRegular   CheckInType = "regular"
	CheckInVolunteer CheckInType = "volunteer"
)

type CheckIn struct {
	Id          Id          `json:"id"`
	FlocksIDs   []Id        `json:"flocks_ids"`
	PersonID    Id          `json:"person_id"`
	GroupID     Id          `json:"group_id,omitempty"`
	CheckInType CheckInType `json:"check_in_type"`
	CheckedInAt time.Time   `json:"checked_in_at,omitempty"`
	Comments    string      `json:"comments,omitempty"`
	SourceID    string      `json:"source_id,omitempty"`
	CreatedAt   *time.Time  `json:"created_at,omitempty"`
	UpdatedAt   *time.Time  `json:"updated_at,omitempty"`
}

type CheckInService interface {
	Create(checkin *CheckIn, opts *Options) error
}
