package flocks

func (p *Person) PrimaryGroup() *Group {
	for _, group := range p.Groups {
		if group.Primary {
			return group
		}
	}

	return nil
}
