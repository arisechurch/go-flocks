package mock

import (
	"github.com/golang/mock/gomock"
	"github.com/icrowley/fake"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	"gitlab.com/arisechurch/go-flocks/pkg/mock_flocks"
	"log"
	"math/rand"
	"os"
)

var (
	personIdCounter int = 0
)

func NewMockPersonService() flocks.PersonService {
	logger := log.New(os.Stdout, "flocks-person-service", log.LstdFlags)
	reporter := &MockReporter{logger}
	ctrl := gomock.NewController(reporter)

	client := mock_flocks.NewMockPersonService(ctrl)

	client.EXPECT().
		Search(gomock.Any()).
		DoAndReturn(personServiceSearch).
		AnyTimes()

	return client
}

func newPerson() *flocks.Person {
	id := personIdCounter
	personIdCounter += 1

	return &flocks.Person{
		Id:        flocks.Id(id),
		FirstName: fake.FirstName(),
		LastName:  fake.LastName(),
		Email:     fake.EmailAddress(),
		MobileTel: fake.Phone(),
		Gender:    fake.Gender(),
		Centre:    fake.City(),
		AgeGroup:  fake.Industry(),
	}
}

func personServiceSearch(
	opts *flocks.PersonSearchOptions,
) ([]*flocks.Person, error) {
	count := rand.Intn(25) + 5
	people := make([]*flocks.Person, count)

	for i := 0; i < count; i++ {
		people[i] = newPerson()
		people[i].Groups = newGroups(rand.Intn(4) + 1)
	}

	return people, nil
}
