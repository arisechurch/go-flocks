package mock

import (
	"log"
)

type MockReporter struct {
	logger *log.Logger
}

func (r *MockReporter) Errorf(format string, args ...interface{}) {
	r.logger.Printf(format, args...)
}

func (r *MockReporter) Fatalf(format string, args ...interface{}) {
	r.logger.Printf(format, args...)
}
