package mock

import (
	"github.com/icrowley/fake"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var (
	groupIdCounter int = 0
)

func newGroup() *flocks.Group {
	id := groupIdCounter
	groupIdCounter += 1

	return &flocks.Group{
		Id:            flocks.Id(id),
		Name:          fake.Industry(),
		Category:      "lifegroup",
		CentreName:    fake.City(),
		AgeGroupName:  fake.Industry(),
		LeadersNames:  "Joe & Jill",
		LeadersEmails: "joe@example.com, jill@example.com",
		Latitude:      fake.Latitude(),
		Longitude:     fake.Longitude(),
		Day:           "Wednesday",
		Frequency:     "Fortnightly",
		Time:          "7pm",
		Topic:         "",
		Suburb:        fake.State(),
		GroupType:     "",
	}
}

func newGroups(count int) []*flocks.Group {
	groups := make([]*flocks.Group, count)

	for i := 0; i < count; i++ {
		groups[i] = newGroup()
	}

	return groups
}
