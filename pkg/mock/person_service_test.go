package mock_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	"gitlab.com/arisechurch/go-flocks/pkg/mock"
)

func TestMockPersonService(t *testing.T) {
	ps := mock.NewMockPersonService()

	t.Run("Search", func(t *testing.T) {
		people, err := ps.Search(&flocks.PersonSearchOptions{
			Query: "test",
		})
		if err != nil {
			assert.FailNow(t, err.Error())
		}

		assert.True(t, 1 < len(people))
	})
}
