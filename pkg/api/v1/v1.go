package v1

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"github.com/google/go-querystring/query"
)

const (
	BaseUrl = "https://flocks.arisechurch.com/api/v1"

	Get  = "GET"
	Post = "POST"
)

type Client struct {
	httpClient *http.Client

	BaseUrl string
	ApiKey  string

	CheckIn        *CheckInService
	Person         *PersonService
	Group          *GroupService
	Inactivation   *InactivationService
	RosterPosition *RosterPositionService
}

func New(apiKey string) *Client {
	c := &Client{
		httpClient: &http.Client{
			Timeout: 60 * time.Second,
		},
		BaseUrl: BaseUrl,
		ApiKey:  apiKey,
	}

	c.CheckIn = &CheckInService{client: c}
	c.Person = &PersonService{client: c}
	c.Group = &GroupService{client: c}
	c.Inactivation = &InactivationService{client: c}
	c.RosterPosition = &RosterPositionService{client: c}

	return c
}

type GetOptions struct {
}

func (c *Client) createUrl(path string) string {
	url := bytes.Buffer{}
	url.WriteString(c.BaseUrl)
	url.WriteString(path)

	return url.String()
}

func (c *Client) createRequest(
	method string,
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	reqUrl := c.createUrl(path)

	req, err := http.NewRequest(method, reqUrl, body)
	if err != nil {
		return nil, err
	}

	qs, err := query.Values(opt)
	if err != nil {
		return nil, err
	}
	qs.Add("api_key", c.ApiKey)
	req.URL.RawQuery = qs.Encode()

	return req, nil
}

func (c *Client) MaybeSetBoolParam(vals *url.Values, key string, val *bool) {
	if val == nil {
		return
	}

	if *val {
		vals.Add(key, "true")
	} else {
		vals.Add(key, "false")
	}
}
func (c *Client) MaybeSetStringParam(vals *url.Values, key string, val *string) {
	if val != nil && *val != "" {
		vals.Add(key, *val)
	}
}

func (c *Client) Do(req *http.Request) (*http.Response, error) {
	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode >= 400 {
		defer res.Body.Close()
		return nil, errors.New(fmt.Sprintf("http error: %d", res.StatusCode))
	}

	return res, nil
}

func (c *Client) Get(path string, opt interface{}) (*http.Request, error) {
	return c.createRequest(Get, path, nil, opt)
}

func (c *Client) GetDo(path string, opt interface{}) (*http.Response, error) {
	req, err := c.Get(path, opt)
	if err != nil {
		return nil, err
	}

	return c.Do(req)
}

func (c *Client) Post(
	path string,
	body io.Reader,
	opt interface{},
) (*http.Request, error) {
	return c.createRequest(Post, path, body, opt)
}

func (c *Client) PostDo(
	path string,
	body io.Reader,
	opt interface{},
) (*http.Response, error) {
	req, err := c.Post(path, body, opt)
	if err != nil {
		return nil, err
	}

	return c.Do(req)
}

func (c *Client) PostJson(
	path string,
	body interface{},
	opt interface{},
) (*http.Response, error) {
	bodyBuf := &bytes.Buffer{}

	if err := json.NewEncoder(bodyBuf).Encode(body); err != nil {
		return nil, err
	}

	req, err := c.Post(path, bodyBuf, opt)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-type", "application/json")

	return c.Do(req)
}
