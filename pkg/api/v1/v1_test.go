package v1_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	api "gitlab.com/arisechurch/go-flocks/pkg/api/v1"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

type Server struct {
	Server  *httptest.Server
	Handler *Handler
}

type Handler struct {
	http.Handler

	HandlerFn func(w http.ResponseWriter, r *http.Request)
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.HandlerFn(w, r)
}

func MustOpenServerClient() (*Server, *api.Client) {
	h := &Handler{}
	s := &Server{
		Server:  httptest.NewServer(h),
		Handler: h,
	}

	c := api.New("foobar")
	c.BaseUrl = s.Server.URL

	return s, c
}

func testV1_Client_GetDo(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(w http.ResponseWriter, r *http.Request) {
		handled = true
		fmt.Fprintf(w, "hello")

		assert.Equal("/test", r.URL.Path)

		params := r.URL.Query()
		assert.Equal("foobar", params.Get("api_key"))
		assert.Equal("123", params.Get("arise_id"))
	}

	res, err := c.GetDo("/test", &flocks.Options{AriseId: 123})
	defer res.Body.Close()

	assert.True(handled)
	assert.Nil(err)
	assert.Equal(http.StatusOK, res.StatusCode)
	resBody, err := ioutil.ReadAll(res.Body)
	assert.Nil(err)
	assert.Equal("hello", string(resBody))
}

func testV1_Client_GetDo_Non200Status(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(w http.ResponseWriter, r *http.Request) {
		handled = true

		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "hello")
	}

	res, err := c.GetDo("/test", &flocks.Options{})

	assert.True(handled)
	assert.NotNil(err)
	assert.Nil(res)
	assert.Equal("http error: 401", err.Error())
}

func TestV1_Client_GetDo(t *testing.T) {
	t.Run("OK", testV1_Client_GetDo)
	t.Run("Non200Status", testV1_Client_GetDo_Non200Status)
}

// Test `Get`
func testV1_ClientGet(t *testing.T) {
	assert := assert.New(t)

	c := api.New("foobar")
	req, err := c.Get("/test", &flocks.Options{AriseId: 123})

	assert.Nil(err)
	assert.NotNil(req)

	assert.Equal(
		"https://flocks.arisechurch.com/api/v1/test?api_key=foobar&arise_id=123",
		req.URL.String(),
	)
}

func TestV1_ClientGet(t *testing.T) {
	t.Run("OK", testV1_ClientGet)
}

func TestV1_ClientPostJson(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(w http.ResponseWriter, r *http.Request) {
		handled = true

		rp := &flocks.RosterPosition{}
		err := json.NewDecoder(r.Body).Decode(rp)
		assert.Nil(err)
		assert.Equal(321, int(rp.PersonId))
		assert.Equal("Test", rp.ServiceName)

		assert.Equal("/test", r.URL.Path)

		params := r.URL.Query()
		assert.Equal("foobar", params.Get("api_key"))
		assert.Equal("123", params.Get("arise_id"))

		fmt.Fprintf(w, "hello")
	}

	res, err := c.PostJson("/test", &flocks.RosterPosition{
		PersonId:    flocks.Id(321),
		ServiceName: "Test",
	}, &flocks.Options{AriseId: 123})
	defer res.Body.Close()

	assert.True(handled)
	assert.Nil(err)
	assert.Equal(http.StatusOK, res.StatusCode)

	resBody, err := ioutil.ReadAll(res.Body)
	assert.Nil(err)
	assert.Equal("hello", string(resBody))
}
