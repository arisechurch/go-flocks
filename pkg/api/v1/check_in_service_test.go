package v1_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

type testCheckIn struct {
	ID          flocks.Id   `json:"id"`
	FlocksIDs   []flocks.Id `json:"flocks_ids"`
	PersonID    flocks.Id   `json:"person_id"`
	GroupID     flocks.Id   `json:"group_id"`
	CheckInType string      `json:"check_in_type"`
	CheckedInAt time.Time   `json:"checked_in_at,omitempty"`
	CreatedAt   *time.Time  `json:"created_at,omitempty"`
	UpdatedAt   *time.Time  `json:"updated_at,omitempty"`
}

func TestCheckInServiceCreate(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	now := time.Now()

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/check_ins/update_or_create", r.URL.Path)

		resRp := &struct {
			CheckIn *testCheckIn `json:"check_in"`
		}{}
		err := json.NewDecoder(r.Body).Decode(resRp)
		assert.Nil(err)
		assert.Equal("volunteer", resRp.CheckIn.CheckInType)
		assert.Equal(now.Unix(), resRp.CheckIn.CheckedInAt.Unix())
		assert.Equal(1, int(resRp.CheckIn.GroupID))
		assert.Equal([]flocks.Id{1, 2, 3}, resRp.CheckIn.FlocksIDs)

		err = json.NewEncoder(w).Encode(&flocks.CheckIn{
			Id: flocks.Id(123),
		})
		assert.Nil(err)

		handled = true
	}

	err := c.CheckIn.Create(&flocks.CheckIn{
		CheckInType: flocks.CheckInVolunteer,
		FlocksIDs:   []flocks.Id{1, 2, 3},
		GroupID:     flocks.Id(1),
		CheckedInAt: now,
	}, &flocks.Options{})

	assert.True(handled)
	assert.Nil(err)
}
