package v1

import (
	"encoding/json"
	"fmt"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var _ flocks.GroupService = &GroupService{}

type GroupService struct {
	client *Client
}

func (gs *GroupService) Get(
	id flocks.Id,
	opt *flocks.Options,
) (*flocks.Group, error) {
	res, err := gs.client.GetDo(
		fmt.Sprintf("/groups/%d", id),
		opt,
	)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	group := &flocks.Group{}
	if err := json.NewDecoder(res.Body).Decode(group); err != nil {
		return nil, err
	}

	return group, nil
}

func (gs *GroupService) List(
	opt *flocks.GroupListOptions,
) ([]*flocks.Group, error) {
	res, err := gs.client.GetDo("/groups", opt)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	groups := []*flocks.Group{}
	if err := json.NewDecoder(res.Body).Decode(&groups); err != nil {
		return nil, err
	}

	return groups, nil
}
