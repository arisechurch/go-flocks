package v1

import (
	"encoding/json"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var _ flocks.InactivationService = &InactivationService{}

type InactivationService struct {
	client *Client
}

func (gs *InactivationService) List(
	opt *flocks.InactivationListOptions,
) ([]*flocks.Inactivation, error) {
	res, err := gs.client.GetDo("/inactivations", opt)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	inactivations := []*flocks.Inactivation{}
	if err := json.NewDecoder(res.Body).Decode(&inactivations); err != nil {
		return nil, err
	}

	return inactivations, nil
}
