package v1

import (
	"encoding/json"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var _ flocks.RosterPositionService = &RosterPositionService{}

type RosterPositionService struct {
	client *Client
}

type rosterPostitionCreateRequest struct {
	RosterPosition *flocks.RosterPosition `json:"roster_position"`
}

func (ps *RosterPositionService) Create(
	rp *flocks.RosterPosition,
	opt *flocks.Options,
) (*flocks.RosterPosition, error) {
	jsonReq := &rosterPostitionCreateRequest{
		RosterPosition: rp,
	}

	res, err := ps.client.PostJson(
		"/roster_positions",
		jsonReq,
		opt,
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	resRp := &flocks.RosterPosition{}
	if err := json.NewDecoder(res.Body).Decode(resRp); err != nil {
		return nil, err
	}

	return resRp, nil
}
