package v1

import (
	"encoding/json"
	"fmt"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var _ flocks.PersonService = &PersonService{}

type PersonService struct {
	client *Client
}

func (ps *PersonService) Get(
	id flocks.Id,
	opt *flocks.Options,
) (*flocks.Person, error) {
	res, err := ps.client.GetDo(
		fmt.Sprintf("/people/%d", id),
		opt,
	)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	person := &flocks.Person{}
	if err := json.NewDecoder(res.Body).Decode(person); err != nil {
		return nil, err
	}

	return person, nil
}

func (ps *PersonService) Search(
	opt *flocks.PersonSearchOptions,
) ([]*flocks.Person, error) {
	res, err := ps.client.GetDo("/people/search", opt)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	people := []*flocks.Person{}
	if err := json.NewDecoder(res.Body).Decode(&people); err != nil {
		return nil, err
	}

	return people, nil
}
