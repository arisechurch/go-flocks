package v1

import (
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

var _ flocks.CheckInService = &CheckInService{}

// CheckInService implements flocks.CheckInService
type CheckInService struct {
	client *Client
}

type checkInCreateRequest struct {
	CheckIn *flocks.CheckIn `json:"check_in"`
}

// Create : POST /api/v1/check_ins
func (s *CheckInService) Create(
	checkin *flocks.CheckIn,
	opt *flocks.Options,
) error {
	jsonReq := &checkInCreateRequest{
		CheckIn: checkin,
	}

	_, err := s.client.PostJson(
		"/check_ins/update_or_create",
		jsonReq,
		opt,
	)
	if err != nil {
		return err
	}

	return nil
}
