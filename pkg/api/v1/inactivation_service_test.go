package v1_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"fmt"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	"net/http"
	"net/url"
	"time"
)

func TestInactivationService_List(t *testing.T) {
	assert := assert.New(t)
	now := time.Now()

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		fmt.Println(r.URL.Query().Encode())
		url.Parse(r.URL.String())
		assert.Equal(
			time.Now().Format(time.RFC3339),
			r.URL.Query().Get("start_updated_at"),
		)

		err := json.NewEncoder(w).Encode([]*flocks.Inactivation{
			{PersonId: 1},
			{PersonId: 2},
		})
		assert.Nil(err)
	}

	inactivations, err := c.Inactivation.List(&flocks.InactivationListOptions{
		StartUpdatedAt: now,
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(inactivations)
	assert.Equal(flocks.Id(1), inactivations[0].PersonId)
	assert.Equal(flocks.Id(2), inactivations[1].PersonId)
}
