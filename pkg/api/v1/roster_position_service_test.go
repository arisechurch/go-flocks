package v1_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	"net/http"
	"time"
)

func testRosterPositionService_Create(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	now := time.Now()

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/roster_positions", r.URL.Path)

		resRp := &struct {
			RosterPosition *flocks.RosterPosition `json:"roster_position"`
		}{}
		err := json.NewDecoder(r.Body).Decode(resRp)
		assert.Nil(err)
		assert.Equal(flocks.TimeToDate(now), resRp.RosterPosition.RosteredAt)
		assert.Equal(1, int(resRp.RosterPosition.PersonId))
		assert.Equal("Test", resRp.RosterPosition.ServiceName)

		err = json.NewEncoder(w).Encode(&flocks.RosterPosition{
			Id: flocks.Id(123),
		})
		assert.Nil(err)

		handled = true
	}

	rp, err := c.RosterPosition.Create(&flocks.RosterPosition{
		RosteredAt:  flocks.Date(now),
		PersonId:    flocks.Id(1),
		ServiceName: "Test",
	}, &flocks.Options{})

	assert.True(handled)
	assert.Nil(err)
	assert.NotNil(rp)
	assert.Equal(123, int(rp.Id))
}

func TestRosterPositionService_Create(t *testing.T) {
	t.Run("OK", testRosterPositionService_Create)
}
