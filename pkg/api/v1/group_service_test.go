package v1_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"encoding/json"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	"net/http"
)

func TestGroupService_Get(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/groups/123", r.URL.Path)

		err := json.NewEncoder(w).Encode(&flocks.Group{
			Name: "Party",
		})
		assert.Nil(err)

		handled = true
	}

	group, err := c.Group.Get(123, &flocks.Options{})

	assert.True(handled)
	assert.Nil(err)
	assert.NotNil(group)
	assert.Equal("Party", group.Name)
}

func TestGroupService_List(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal(
			"/groups?api_key=foobar&category=lifegroup",
			r.URL.String(),
		)

		err := json.NewEncoder(w).Encode([]*flocks.Group{
			{Name: "Party 1"},
			{Name: "Party 2"},
		})
		assert.Nil(err)
	}

	groups, err := c.Group.List(&flocks.GroupListOptions{
		Category: "lifegroup",
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(groups)
	assert.Equal("Party 1", groups[0].Name)
	assert.Equal("Party 2", groups[1].Name)
}
