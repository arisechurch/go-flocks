package v1_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"encoding/json"
	"net/http"

	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
)

func testPersonService_Get(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		assert.Equal("/people/123", r.URL.Path)

		err := json.NewEncoder(w).Encode(&flocks.Person{
			FirstName: "John",
		})
		assert.Nil(err)

		handled = true
	}

	person, err := c.Person.Get(123, &flocks.Options{})

	assert.True(handled)
	assert.Nil(err)
	assert.NotNil(person)
	assert.Equal("John", person.FirstName)
}

func TestPersonService_Get(t *testing.T) {
	t.Run("OK", testPersonService_Get)
}

func TestPersonService_Search(t *testing.T) {
	assert := assert.New(t)

	s, c := MustOpenServerClient()
	handled := false

	s.Handler.HandlerFn = func(
		w http.ResponseWriter,
		r *http.Request,
	) {
		handled = true

		assert.Equal("/people/search", r.URL.Path)

		params := r.URL.Query()
		assert.Equal("john", params.Get("q"))
		assert.Equal("123", params.Get("arise_id"))
		assert.Len(params["group_ids"], 3)
		assert.Equal("3", params["group_ids"][2])

		err := json.NewEncoder(w).Encode([]*flocks.Person{
			{FirstName: "John"},
			{FirstName: "Jonny"},
		})
		assert.Nil(err)
	}

	people, err := c.Person.Search(&flocks.PersonSearchOptions{
		AriseId:  123,
		Query:    "john",
		GroupIds: []string{"1", "2", "3"},
	})

	if !assert.True(handled) {
		return
	}

	assert.Nil(err)
	assert.NotNil(people)
	assert.NotNil(people[1])
	assert.Equal("John", people[0].FirstName)
	assert.Equal("Jonny", people[1].FirstName)
}
