# go-flocks

[![GoDoc](https://godoc.org/gitlab.com/arisechurch/go-flocks?status.svg)](https://godoc.org/gitlab.com/arisechurch/go-flocks)

## Usage

```go
package main

import (
	"fmt"
	"gitlab.com/arisechurch/go-flocks/pkg/flocks"
	api "gitlab.com/arisechurch/go-flocks/pkg/api/v1"
)

func main() {
	c := api.New("api_key_goes_here")

	person, _ := c.Person.Get(123, &flocks.Options{})

	fmt.Printf("Hi, %s!", person.FirstName)
}
```
