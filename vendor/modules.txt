# github.com/corpix/uarand v0.1.1
## explicit
github.com/corpix/uarand
# github.com/davecgh/go-spew v1.1.1
## explicit
github.com/davecgh/go-spew/spew
# github.com/golang/mock v1.4.4-0.20200911215831-eb4f98929efd
## explicit
github.com/golang/mock/gomock
# github.com/google/go-querystring v1.0.0
## explicit
github.com/google/go-querystring/query
# github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
## explicit
github.com/icrowley/fake
# github.com/pmezard/go-difflib v1.0.0
## explicit
github.com/pmezard/go-difflib/difflib
# github.com/stretchr/testify v1.6.1
## explicit
github.com/stretchr/testify/assert
# gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
## explicit
gopkg.in/yaml.v3
