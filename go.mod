module gitlab.com/arisechurch/go-flocks

go 1.15

require (
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/mock v1.4.4-0.20200911215831-eb4f98929efd
	github.com/google/go-querystring v1.0.0
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
